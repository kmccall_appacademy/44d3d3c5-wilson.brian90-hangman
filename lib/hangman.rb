class Hangman
  attr_reader :guesser, :referee, :board
  attr_accessor :computer
  attr_accessor :secret_word_length
  attr_accessor :guessed_letters


  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup

    @secret_word_length = @referee.pick_secret_word
    @guesser.register_secret_length(@secret_word_length)
    @board = Array.new(@secret_word_length, "_")
    @guessed_letters = []
  end

  def register_word
    (@referee.pick_secret_word).times @board += "_"
  end

  def take_turn
    letter = @guesser.guess
    response = @referee.check_guess(letter)
    @guesser.handle_response(letter, response)
    update_board(response, letter)
  end

  def update_board(indices, letter)
    indices.each do |idx|
      @board[idx] = letter
    end
  end


end


class HumanPlayer
  def register_secret_length(len)
    @secret_word_length = len
  end

  def guess
    puts "Pick a letter!"
    letter = gets.chomp
    letter
  end

  def handle_response
  end

end


class ComputerPlayer
  attr_accessor :dictionary
  attr_accessor :candidate_words
  attr_accessor :letters
  attr_accessor :word
  attr_accessor :guessed_letters

  def register_secret_length(len)
    @secret_word_length = len
    @candidate_words.each do |word|
      if word.length != len
        @candidate_words.delete(word)
      end
    end

  end

  def initialize(dictionary)
    if dictionary.length == 0
      dictionary = generate_dictionary
    end
    @dictionary = dictionary
    @candidate_words = dictionary.clone()
    @guessed_letters = []

    puts "dictionary len with comp player init: " + dictionary.length.to_s

    update_letters
  end

  def update_letters
    hash = {}
    hash.default = 0

    @candidate_words.join.chars.each do |letter|
      hash[letter] += 1 if !@guessed_letters.include?(letter)
    end

    hash = hash.sort_by{|k, v| v}.to_h

    @letters = hash.keys
    puts "letters guessed:" + @guessed_letters.to_s
    puts "hash:" + hash.to_s
  end

  def generate_dictionary
    dictionary = []
    File.foreach("dictionary.txt") {|line| dictionary << line.chomp}
    puts "dictionary length: " + dictionary.length.to_s
    dictionary
  end

  def pick_secret_word
    @word = @dictionary[(@dictionary.length * rand).floor]
    @word.length
  end

  def guess(board)
    board.each{|l| guessed_letters << l}
    update_letters
    letter = @letters.pop
    guessed_letters << letter
    letter
  end

  def handle_response(letter, response)

    if response.length > 0
      @candidate_words.each do |candidate|
        candidate_array = candidate.chars
        response.each do |idx|
          appearances = []
          candidate_array.each_with_index do |char, idx2|
            appearances << idx2 if letter == char
          end

          if response != appearances
            @candidate_words.delete(candidate)
          end
        end
      end
    end

    if response.length == 0
      @candidate_words.each{|c| @candidate_words.delete(c) if c.include?(letter)}
    end

    update_letters

  end

  def check_guess(letter)
    indices = []
    @word.chars.each_with_index do |char, idx|
      if letter == char
        indices << idx
      end
    end
    indices
  end

end
